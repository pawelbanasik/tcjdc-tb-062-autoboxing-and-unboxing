package com.pawelbanasik;

import java.util.ArrayList;

class IntClass {

	private int myValue;

	public IntClass(int myValue) {
		super();
		this.myValue = myValue;
	}

	public int getMyValue() {
		return myValue;
	}

	public void setMyValue(int myValue) {
		this.myValue = myValue;
	}

}

public class Main {

	public static void main(String[] args) {

		String[] strArray = new String[10];
		int[] intArray = new int[10];

		// ze stringiem dziala bo String to nie primitive type tylko klasa
		ArrayList<String> strArrayList = new ArrayList<String>();
		strArrayList.add("Tim");

		// nie da sie tego zrobic w taki sposob bo int to primitive type
		// ArrayList<int> intArrayList = new ArrayList<int> ();

		// mozna to zrobic w taki sposob ale tak sie nie robi bo to
		// skomplikowane
		ArrayList<IntClass> intClassArrayList = new ArrayList<IntClass>();
		intClassArrayList.add(new IntClass(54));

		// wtedy wchodzi autoboxing zeby to zrobic jak nalezy
		// wbudowane klasy prymitywnych danych
		Integer integer = new Integer(54);
		Double doubleValue = new Double(12.25);

		ArrayList<Integer> intArrayList = new ArrayList<Integer>();

		for (int i = 0; i <= 10; i++) {
			// tu sie dzieje autoboxing czyli tworzymy/zamieniamy primitive type
			// int w Integer (to w nawiasie poni�ej)
			// "Object Wrapper"
			intArrayList.add(Integer.valueOf(i));

		}

		for (int i = 0; i <= 10; i++) {
			// to jest unboxing (to w nawiasie po plusie) - zamieniamy znowu w
			// primitive type
			System.out.println(i + " --> " + intArrayList.get(i).intValue());

		}

		// java ma taki skrot zamiast pisac ciagle new
		Integer myIntValue = 56; // zamiast tego skr�t po lewej dzia�a
									// Integer.valueOf(56);

		int myInt = myIntValue.intValue(); // myIntValue.intValue();

		ArrayList<Double> myDoubleValues = new ArrayList<Double>();
		for (double dbl = 0.0; dbl <= 10.0; dbl += 0.5) {
			// myDoubleValues.add(Double.valueOf(dbl));
			myDoubleValues.add(dbl);
		}
		for (int i = 0; i < myDoubleValues.size(); i++) {

			double value = myDoubleValues.get(i).doubleValue();
			System.out.println(i + " --> " + value);

		}
	}

}
